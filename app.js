const mqtt = require("mqtt");
const mysql = require("mysql");
const dateFormat = require('dateformat');;

const pool = mysql.createPool({
	connectionLimit : 10
	, host: 'localhost'
	, user: 'root'
	, password: ''
	, database: 'teamsflare'
});

const client = mqtt.connect({
	port: 1883
	, host: 'localhost'
});

client.on("connect", () => {
	console.log("Connected to MQTT");
	client.subscribe('messages/unsaved');
});

client.on("message", (topic, message) => {
	let jsonMessage = JSON.parse(message)
		, now = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");

	console.log(topic);

	pool.getConnection(function(err, connection) {
		// Use the connection
		connection.query('\
			INSERT INTO `messages`\
			(`author_id`, `receiver_id`, `body`, `created_at`, `updated_at`)\
			VALUES\
			(' + jsonMessage.from + ', ' + jsonMessage.to + ', "' + jsonMessage.message + '", "' + now + '", "' + now + '")\
		', function (error, results, fields) {
			console.log('Message saved');
			message = JSON.stringify({
				id: results.insertId,
				author_id: jsonMessage.from,
				receiver_id: jsonMessage.to,
				body: jsonMessage.message,
				created_at: now,
				updated_at: now
			});

			client.publish('messages/saved/' + jsonMessage.to, message, (err) => {
				if (err) {
					throw err;
				}
				
				console.log('Message published (messages/saved/' + jsonMessage.to + ')');
				console.log('Message: ');
				console.log(message);
			});

			connection.release();

			if (error) throw error;
		});
	});

	console.log("Message received: " + message);
});